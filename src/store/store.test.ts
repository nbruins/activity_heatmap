import { totalDistanceInKm, tracks } from '../test_mock_data'
import { defaultFilter, store } from './store'

describe('test easy-peasy store', () => {

  it('should load the store with the default values', () => {

    expect(store.getState().activityTypes).toHaveLength(0)
    expect(store.getState().tracks).toHaveLength(0)
    expect(store.getState().loading).toBeTruthy()
    expect(store.getState().filter).toMatchObject(defaultFilter)

  })

  it('should compute the total distance correct', () => {

    const addTracksToStore = store.getActions().addTracksToStore

    addTracksToStore({ tracks })
    expect(store.getState().tracks).toHaveLength(5)
    expect(store.getState().totalDistanceInKm).toEqual(totalDistanceInKm)

  })
})

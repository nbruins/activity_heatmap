import { startOfYear } from 'date-fns'
import { action, Action, computed, Computed, createStore, thunkOn, ThunkOn } from 'easy-peasy'
import { fetchApi, mapService } from '../services'
import { Filter, Totals, TrackData, TrackPoints } from '../types'

export interface StoreModel {
  activityTypes: string[]
  tracks: TrackData[]
  totals: Totals[]
  filter: Filter
  loading: boolean
  init: Action<StoreModel>
  onInit: ThunkOn<StoreModel, undefined, StoreModel>
  setLoading: Action<StoreModel, boolean>
  addActivityTypes: Action<StoreModel, string[]>
  updateFilter: Action<StoreModel, Filter>
  onUpdateFilter: ThunkOn<StoreModel, undefined, StoreModel>
  addTracksToStore: Action<StoreModel, { tracks: TrackData[] }>
  addPointsToMap: Action<StoreModel, { points: TrackPoints[] }>
  addTotalsToStore: Action<StoreModel, { totals: Totals[] }>
  totalDistanceInKm: Computed<StoreModel, number>
}
export const defaultFilter: Filter = {
  dateFrom: startOfYear(new Date()),
  dateTo: new Date(),
  type: null,
  distanceRange: null
}

export const store = createStore<StoreModel>({
  activityTypes: [],
  tracks: [],
  totals: [],
  filter: defaultFilter,
  loading: true,
  setLoading: action((state, payload) => { state.loading = payload }),
  init: action(() => { mapService.initMap() }),
  onInit: thunkOn(
    (actions) => actions.init,
    (actions, _target, { getState }) => {

      fetchApi.getAllActivityTypes()
        .then(activityTypes => {
          actions.addActivityTypes(activityTypes)
          const type = activityTypes.includes('Running') ? 'Running' : activityTypes[0]
          actions.updateFilter({ ...getState().filter, type })
        })
        .catch((err: unknown) => { console.log(err) })

      fetchApi.getAllTotals()
        .then(totals => { actions.addTotalsToStore({ totals }) })
        .catch((err: unknown) => { console.log(err) })
    }
  ),
  addActivityTypes: action((state, activityTypes) => {
    state.activityTypes = activityTypes
  }),
  updateFilter: action((state, filter) => {
    state.filter = filter
  }),
  onUpdateFilter: thunkOn(
    (actions) => [actions.init, actions.updateFilter],
    (actions, target) => {
      if (target.payload) {
        actions.setLoading(true)
        Promise.all([
          fetchApi.getTrackData(target.payload)
            .then((tracks) => { actions.addTracksToStore({ tracks }) }),
          fetchApi.getTrackPoints(target.payload)
            .then((points) => { actions.addPointsToMap({ points }) })
        ]).then(() => { actions.setLoading(false) })
          .catch((err: unknown) => { console.log(err) })
      }
    }
  ),
  addTracksToStore: action((state, { tracks }) => {
    state.tracks = tracks
  }),
  addPointsToMap: action((_state, { points }) => {
    mapService.addTracksToMap(points)
  }),
  addTotalsToStore: action((state, { totals }) => {
    state.totals = totals
  }),
  totalDistanceInKm: computed(state => state.tracks.reduce((sum, track) => sum + track.distanceInKm, 0))
})


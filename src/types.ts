import * as L from 'leaflet'

export interface TrackData {
  id: number
  timestamp: string,
  activityType: string
  distanceInKm: number
  averageSpeedInMPerS?: number
  averageHearthRate?: number
  duration?: string
}

export type Point = [number, number]

export interface TrackPoints {
  id: number,
  points: Point[]
}

export interface ExtractedTrack extends Omit<TrackData, 'id'> {
  points: Point[]
}

export enum SupportedFileFormats {
  fit = 'fit',
  gpx = 'gpx',
  tcx = 'tcx'
}

type ExtractFunction = (file: File) => Promise<ExtractedTrack | null>

export type ExtractTracks = Record<SupportedFileFormats | string, ExtractFunction>

export interface Filter {
  dateFrom: Date | null
  dateTo: Date | null
  type: string | null
  distanceRange: number[] | null
}

export type FilterKeys = keyof Filter

export interface BaseLayersFromDb {
  name: string
  url: string
  options?: L.TileLayerOptions
}

export interface Totals {
  year: number
  activityType: string
  total: number
  numberOf: number
}
import CssBaseline from '@mui/material/CssBaseline'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import { createRoot } from 'react-dom/client'
import App from './App'
import './i18n/i18n'
import { store } from './store'
import './styles.css'

// A custom theme for this app
const theme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      light: '#4c8c4a',
      main: '#75aa51',
      dark: '#003300',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ffe97d',
      main: '#ffb74d',
      dark: '#c88719',
      contrastText: '#fff',
    },
    background: {
      default: '#1b2d00',
      paper: '#083b0d',
    }
  }
})


// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const container = document.getElementById('root')!

const root = createRoot(container)
root.render(
  <StoreProvider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </StoreProvider >
)

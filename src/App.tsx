import {
  CircularProgress, styled, Typography
} from '@mui/material'
import React, { DragEvent, FC, useEffect, useState } from 'react'
import { LinearProgressWithLabel, TracksTable } from './component'
import { ActivityTypeSelect, Summary } from './containers'
import { fetchApi, processFile } from './services'
import { useStoreActions, useStoreState } from './store'
import { ExtractedTrack } from './types'


const Main = styled('div')({
  height: '100%',
  minHeight: '100%',
  display: 'flex',
  flexDirection: 'column'
})

const Container = styled('div')(({ theme }) => ({
  flex: '1 1',
  order: 2,
  display: 'flex',
  height: '100%',
  flexDirection: 'row',
  [theme.breakpoints.down('md')]: {
    flexDirection: 'column',
  }
}))

const MainBody = styled('div')({ flex: 1, flexDirection: 'column' })

const MapLayer = styled('div')({ flex: 1, height: 'calc(100% - 240px)' })

const LoadingProgressContainer = styled('div')({
  position: 'absolute',
  left: '50%'
})

const handleDragOver = (event: DragEvent): void => {
  event.dataTransfer.dropEffect = 'copy'
  event.stopPropagation()
  event.preventDefault()
}

const App: FC = () => {

  const [loading, filter] = useStoreState(state => ([state.loading, state.filter]))

  const init = useStoreActions(actions => actions.init)
  const updateFilter = useStoreActions(actions => actions.updateFilter)

  const [progress, setProgress] = useState(0)

  useEffect(() => { init() }, [init])

  const handleUpload = (event: DragEvent) => {
    event.stopPropagation()
    event.preventDefault()

    const files = Array.from(event.dataTransfer.files)
    const noOfFiles = files.length
    let noOfFilesProcessed = 0
    const uploadedTracks: ExtractedTrack[] = []
    void Promise.all(
      files.map((file) =>
        processFile(file)
          .then(track => {
            if (track) uploadedTracks.push(track)
          })
          .then(() => {
            noOfFilesProcessed += 1
            setProgress((noOfFilesProcessed / noOfFiles) * 100)
          })
      )
    ).then(() => {
      void fetchApi.addTracks(uploadedTracks)
        .then(() => {
          updateFilter(filter)
          setProgress(0)
        })
    })
  }

  return (
    <Main>
      {progress > 0 &&
        <LoadingProgressContainer>
          <LinearProgressWithLabel value={progress} />
        </LoadingProgressContainer>
      }
      {loading &&
        <LoadingProgressContainer>
          <CircularProgress color='success' />
          <Typography component="p" >Loading</Typography>
        </LoadingProgressContainer>
      }
      <Container>
        <Summary />
        <MainBody>
          <MapLayer
            id="mapLayer"
            onDragOver={handleDragOver}
            onDrop={handleUpload}
          >
            <ActivityTypeSelect />
          </MapLayer>
          <TracksTable />
        </MainBody>
      </Container>
    </Main>
  )
}

export default App

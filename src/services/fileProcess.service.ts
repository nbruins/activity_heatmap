import { SportsLib } from '@sports-alliance/sports-lib'
import { EventInterface } from '@sports-alliance/sports-lib/lib/events/event.interface'
import { format } from 'date-fns'
import { ExtractTracks, Point, SupportedFileFormats, ExtractedTrack } from '../types'

const readFile = (file: File, binairy = false): Promise<unknown> => new Promise((resolve, reject) => {
  const reader = new FileReader()
  reader.onload = res => { resolve(res.target?.result) }
  // eslint-disable-next-line @typescript-eslint/prefer-promise-reject-errors
  reader.onerror = err => { reject(err) }
  // eslint-disable-next-line @typescript-eslint/no-unused-expressions
  binairy ? reader.readAsArrayBuffer(file) : reader.readAsText(file)
})

const processEvent = (event: EventInterface): Promise<ExtractedTrack | null> => {
  const activity = event.getFirstActivity()

  try {

    const positionData = activity.getPositionData()

    if (positionData.length <= 0) {

      console.log('File has no records!', activity)

    } else {

      const points: Point[] = []
      positionData.forEach(record => {
        if (record?.latitudeDegrees && record.longitudeDegrees)
          points.push([record.latitudeDegrees, record.longitudeDegrees])
      })

      const distance = activity.getDistance()
      const distanceValue = Number(distance.getDisplayValue())
      const distanceUnit = distance.getDisplayUnit()
      const distanceInKm = distanceUnit === 'Km' ? distanceValue : distanceValue / 1000
      const duration = activity.getDuration().getDisplayValue()
      const averageHearthRate = activity.getStats().get('Average Heart Rate')?.getValue() as number
      const averageSpeedInMPerS = activity.getStats().get('Average Speed')?.getValue() as number

      const track: ExtractedTrack | null = points.length > 0 ? {
        timestamp: format(activity.startDate, 'yyyy-MM-dd HH:mm:SS'),
        points,
        activityType: event.getActivityTypesAsString(),
        distanceInKm,
        duration,
        averageSpeedInMPerS,
        averageHearthRate
      } : null
      return Promise.resolve(track)
    }
  } catch (err) {
    console.log(err)
  }
  return Promise.resolve(null)
}

const extractTracks: ExtractTracks = {
  fit: (file: File) => readFile(file, true)
    .then(contents => SportsLib.importFromFit((contents as ArrayBuffer))
      .then((event) => processEvent(event))),
  gpx: (file: File) => readFile(file)
    .then(contents => SportsLib.importFromGPX((contents as string))
      .then((event) => processEvent(event))),
  tcx: (file: File) => readFile(file)
    .then(contents => SportsLib.importFromTCX((new DOMParser()).parseFromString((contents as string), 'application/xml'))
      .then((event) => processEvent(event))),
}

export const processFile = async (file: File): Promise<ExtractedTrack | null> => {
  const fileFormat = file.name.split('.').pop()?.toLowerCase() ?? ''
  if (Object.keys(SupportedFileFormats).includes(fileFormat)) {
    return extractTracks[fileFormat](file)
  } else {
    console.log(`Unsupported file format: ${fileFormat}`)
    return Promise.resolve(null)
  }
}
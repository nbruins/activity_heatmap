import { processFile } from './fileProcess.service'
import path from 'path'
import fs from 'fs'

const createFileObj = (filePath: string): File => {

  const buffer = fs.readFileSync(filePath)
  const arrayBuffer = buffer.subarray(buffer.byteOffset, buffer.byteOffset + buffer.byteLength)

  const name = path.basename(filePath)

  return new File([arrayBuffer], name, { type: 'unknown' })

}


describe('test file process service', () => {

  it.each([{ type: 'fit' }, { type: 'tcx' }, { type: 'gpx' }])('should be able process an $type file', async ({ type }) => {
    const fitFile = createFileObj(path.resolve(__dirname, `../mock/test.${type}`))
    await processFile(fitFile).then((result) => {
      expect(result).toBeDefined()
      expect(result?.activityType).toBe('Running')
      expect(result?.distanceInKm).toBeGreaterThanOrEqual(13.23)
      expect(result?.duration).toBeDefined()
      expect(result?.averageSpeedInMPerS).toBeGreaterThanOrEqual(2.0792342200395217)
      expect(Math.round(result?.averageHearthRate ?? 0)).toBe(142)
      expect(result?.points.length).toBe(6201)
    })
  })
})
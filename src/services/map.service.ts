import * as L from 'leaflet'
import leaflet, { Map, Polyline } from 'leaflet'
import { TrackPoints } from '../types'
import { fetchApi } from './fetchApi'

const lineOptions = {
  color: '#df07f8',
  weight: 1,
  opacity: 1,
  smoothFactor: 1
}

let map: Map | null = null
let isInitialized = false
const lines: Record<number, Polyline> = {}

const initMap = () => {
  if (!isInitialized) {
    isInitialized = true
    if (!map)
      fetchApi.getBaseLayers()
        .then(baseLayersFromDb => {
          const baseLayers = baseLayersFromDb.reduce<Record<string, L.Layer>>((previous, current) => ({
            ...previous,
            [current.name]: L.tileLayer(current.url, current.options)
          }), {})
          map = L.map('mapLayer', {
            center: [52.2563, 7.0161],
            zoom: 10,
            preferCanvas: true,
            dragging: !L.Browser.mobile, tap: !L.Browser.mobile
          })

          const activeLayer = baseLayers[Object.keys(baseLayers)[0]]
          activeLayer.addTo(map)
          L.control.layers(baseLayers).addTo(map)
        })
        .catch((err: unknown) => { console.log(err) })
  }
}

const centerMap = (polylines: Polyline[]) => {
  if (map)
    map.fitBounds(leaflet.featureGroup(polylines).getBounds(), {
      noMoveStart: true,
      animate: false,
      padding: [50, 20],
    })
}

const addTracksToMap = (points: TrackPoints[]): void => {

  // remove all existing lines from map and add new lines
  Object.keys(lines).forEach(trackId => {
    map?.removeLayer(lines[Number(trackId)])
    // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
    delete lines[Number(trackId)]
  })

  points.forEach(trck => {
    if (map)
      lines[trck.id] = leaflet.polyline(trck.points, lineOptions).addTo(map)
  })

  if (Object.keys(lines).length > 0)
    centerMap(Object.values(lines))
}

const selectTrack = (trackId: number): void => {
  if (map) {
    // Remove alle lines and add only the selected line
    Object.keys(lines).forEach(trckId => {
      map?.removeLayer(lines[Number(trckId)])
    })

    lines[trackId].addTo(map)
    centerMap([lines[trackId]])
  }
}

export const mapService = { map, initMap, addTracksToMap, selectTrack }
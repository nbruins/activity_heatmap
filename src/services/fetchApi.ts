import { addDays, format } from 'date-fns'
import { BaseLayersFromDb, ExtractedTrack, Filter, FilterKeys, Totals, TrackData, TrackPoints } from '../types'

const baseUrl = window.location.origin.includes('localhost') ?
  'http://localhost:8080/api.php/records' : `${window.location.origin}/api/api.php/records`

const activityUrl = `${baseUrl}/activity`

const headers = new Headers({ 'Content-Type': 'application/json' })

const fetchGet = <T>(url: string): Promise<T[]> =>
  fetch(url, { headers, mode: 'cors' })
    .then(async (response) => (await response.json()) as { records: T[] })
    .then((response) => response.records)
    .catch((err: unknown) => {
      console.log(err)
      return Promise.resolve([] as T[])
    })

const getAllActivityTypes = (): Promise<string[]> =>
  fetchGet<{ activityType: string }>(`${baseUrl}/activityType?order=activityType,asc`)
    .then(response =>
      response.map(item => (item.activityType)))
    .catch((err: unknown) => {
      console.log(err)
      return Promise.resolve([] as string[])
    })


const addTracks = (tracks: ExtractedTrack[]): Promise<unknown> => {
  const tracksToUpload = tracks.map(track => ({
    ...track,
    timestamp: typeof track.timestamp === 'string' ? track.timestamp : format(track.timestamp, 'yyyy-MM-dd HH:mm:SS'),
  }))
  const body = JSON.stringify(tracksToUpload)
  return fetch(activityUrl, { method: 'POST', headers, body, mode: 'cors' })
}

type AddFilterValuesProps = { [key in FilterKeys]: (search: URLSearchParams, filter: Filter | null) => void }
const addFilterValueToSearchParams: AddFilterValuesProps = {
  dateFrom: (search, filter) => {
    if (filter?.dateFrom) search.append('filter', `timestamp,gt,${format(filter.dateFrom, 'yyyyMMdd')}`)
  },
  dateTo: (search, filter) => {
    if (filter?.dateTo) search.append('filter', `timestamp,lt,${format(addDays(filter.dateTo, 1), 'yyyyMMdd')}`)
  },
  type: (search, filter) => {
    if (filter?.type) search.append('filter', `activityType,eq,${filter.type}`)
  },
  distanceRange: (search, filter) => {
    if (filter?.distanceRange) {
      search.append('filter', `distanceInKm,ge,${filter.distanceRange[0].toString()}`)
      search.append('filter', `distanceInKm,le,${filter.distanceRange[1].toString()}`)
    }
  }
}

interface Props {
  filter: Filter | null
  onlyPoints?: boolean
}
const getTrackDataOrPoints = <T>({ filter, onlyPoints = false }: Props): Promise<T[]> => {

  const searchParams = new URLSearchParams()
  if (filter)
    Object.keys(filter).forEach(key => {
      addFilterValueToSearchParams[key as FilterKeys](searchParams, filter)
    })

  if (onlyPoints) {
    searchParams.append('include', 'id,points')
  } else {
    searchParams.append('exclude', 'points')
    searchParams.append('order', 'timestamp,desc')
  }

  const url = new URL(activityUrl)
  url.search = searchParams.toString()

  return fetchGet<T>(url.toString())
}

const getTrackData = (filter: Filter | null): Promise<TrackData[]> => getTrackDataOrPoints<TrackData>({ filter })

const getTrackPoints = (filter: Filter | null): Promise<TrackPoints[]> => getTrackDataOrPoints<TrackPoints>({ filter, onlyPoints: true })

export const fetchApi = {
  getAllActivityTypes,
  getBaseLayers: (): Promise<BaseLayersFromDb[]> => fetchGet<BaseLayersFromDb>(`${baseUrl}/baseLayers`),
  getAllTotals: (): Promise<Totals[]> => fetchGet<Totals>(`${baseUrl}/totals?order=year,desc`),
  addTracks,
  getTrackData,
  getTrackPoints
}

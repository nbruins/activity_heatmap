import { waitFor } from '@testing-library/react'
import { activityType, baseLayers, extractedTracks, tracks } from '../test_mock_data'
import { fetchApi } from './fetchApi'


describe('test fetchApi service', () => {

  it('should be able to add tracks without error', async () => {

    await fetchApi.addTracks(extractedTracks).then(() => { expect(fetch).toHaveBeenCalledTimes(1); })
  })

  it('should be able to get the base layers', async () => {

    void fetchApi.getBaseLayers()
      .then(response => {
        expect(response).toMatchObject(baseLayers)
      })
    await waitFor(() => { expect(fetch).toHaveBeenCalledTimes(1); })
  })

  it('should be able to get tracks', async () => {

    void fetchApi.getTrackData(null)
      .then(response => {
        expect(response).toMatchObject(tracks)
      })
    await waitFor(() => { expect(fetch).toHaveBeenCalledTimes(1); })
  })

  it('should be able to get all activity types', async () => {

    void fetchApi.getAllActivityTypes()
      .then(response => {
        expect(response).toMatchObject(activityType.map(item => (item.activityType)))
      })
    await waitFor(() => { expect(fetch).toHaveBeenCalledTimes(1); })
  })
})
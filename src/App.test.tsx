import { css, Global, ThemeProvider } from '@emotion/react'
import { createTheme } from '@mui/material'
import { render, screen, waitFor } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import App from './App'
import './i18n/i18n'
import { store } from './store'

const theme = createTheme({
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: { height: '100%' },
        body: { height: '100%' }
      },
    },
  },
})

describe('test if app can be rendered', () => {

  it('should render the app', async () => {

    render(
      <StoreProvider store={store}>
        <ThemeProvider theme={theme}>
          <Global
            styles={css`
            html {height: 100%;}
            body {height: 100%;}
            #root {height: 100%;}
          `}
          />
          <App />
        </ThemeProvider>
      </StoreProvider>
    )
    await waitFor(() => { expect(fetch).toHaveBeenCalledTimes(5) })
    await waitFor(() => { expect(screen.getAllByText('Running').length).toBe(7) })
  })
})
import { ExtractedTrack, Point, TrackData } from './types'

export const points: Point[] = [
  [52.2561614, 7.0157546],
  [52.2561562, 7.0157696],
  [52.2563193, 7.0158853]
]

export const tracks: Required<TrackData>[] = [
  {
    id: 506,
    activityType: 'Running',
    timestamp: '2022-11-04 11:42:00',
    distanceInKm: 10.01,
    averageSpeedInMPerS: 3.019,
    averageHearthRate: 156,
    duration: '55m 15s'
  },
  {
    id: 505,
    activityType: 'Running',
    timestamp: '2022-11-01 19:31:00',
    distanceInKm: 11.58,
    averageSpeedInMPerS: 2.954,
    averageHearthRate: 146,
    duration: '01h 05m 19s'
  },
  {
    id: 503,
    activityType: 'Running',
    timestamp: '2022-10-30 10:08:00',
    distanceInKm: 13,
    averageSpeedInMPerS: 2.753,
    averageHearthRate: 137,
    duration: '01h 18m 43s'
  },
  {
    id: 500,
    activityType: 'Running',
    timestamp: '2022-10-27 19:29:00',
    distanceInKm: 6.15,
    averageSpeedInMPerS: 2.745,
    averageHearthRate: 149,
    duration: '37m 19s'
  },
  {
    id: 499,
    activityType: 'Running',
    timestamp: '2022-10-26 19:06:00',
    distanceInKm: 2.77,
    averageSpeedInMPerS: 1.88,
    averageHearthRate: 110,
    duration: '24m 35s'
  }
]

export const extractedTracks: ExtractedTrack[] = tracks.map(track => ({ ...track, points }))

export const totalDistanceInKm = tracks.reduce((sum, track) => sum + track.distanceInKm, 0)

const attribution = 'test'
export const baseLayers = [
  {
    id: 1,
    name: 'Open Street Map',
    url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    options: {
      maxZoom: 19,
      attribution
    }
  },
  {
    id: 3,
    name: 'Esri World Imagery',
    url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
    options: {
      attribution
    }
  },
  {
    id: 4,
    name: 'nlmaps Luchtfoto',
    url: 'https://service.pdok.nl/hwh/luchtfotorgb/wmts/v1_0/Actueel_ortho25/EPSG:3857/{z}/{x}/{y}.jpeg',
    options: {
      bounds: [
        [
          50.5,
          3.25
        ],
        [
          54,
          7.6
        ]
      ],
      maxZoom: 19,
      minZoom: 6,
      attribution
    }
  },
  {
    id: 5,
    name: 'CartoDB Positron',
    url: 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png',
    options: {
      maxZoom: 20,
      subdomains: 'abcd',
      attribution
    }
  }
]


export const activityType = [
  { activityType: 'Alpine Skiing' },
  { activityType: 'Cycling' },
  { activityType: 'Mountaineering' },
  { activityType: 'Open Water Swimming' },
  { activityType: 'Orienteering' },
  { activityType: 'Running' },
  { activityType: 'Walking' }
]
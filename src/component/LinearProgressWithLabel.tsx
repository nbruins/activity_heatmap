import {
  Box, LinearProgress,
  LinearProgressProps, Typography
} from '@mui/material'
import React from 'react'

export const LinearProgressWithLabel = (props: LinearProgressProps & { value: number }) => (
  <Box display="flex" alignItems="center">
    <Box width="100%" mr={1}>
      <LinearProgress variant="determinate" {...props} />
    </Box>
    <Box minWidth={35}>
      <Typography variant="body2" color="textSecondary">
        {`${Math.round(props.value).toString()}%`}
      </Typography>
    </Box>
  </Box>
)
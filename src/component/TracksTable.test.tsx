import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import '../i18n/i18n'
import { store } from '../store'
import { tracks } from '../test_mock_data'
import { TracksTable } from './TracksTable'
import { mapService } from '../services/map.service'

describe('Test TracksTable component', () => {

  it('should render an empty tracks table', () => {

    render(
      <StoreProvider store={store}>
        <TracksTable />
      </StoreProvider>
    )

    expect(screen.getByText('No data')).toBeDefined()
  })

  it('should render tracks table with activities', async () => {

    const addTracksToStore = store.getActions().addTracksToStore

    addTracksToStore({ tracks })

    const mapServiceSpy = jest.spyOn(mapService, 'selectTrack')

    render(
      <StoreProvider store={store}>
        <TracksTable />
      </StoreProvider>
    )

    expect(screen.getAllByText('Running').length).toBe(5)
    expect(screen.getByText('05:31 (10.9)')).toBeDefined()
    expect(screen.getByText('55m 15s')).toBeDefined()
    expect(screen.getByText('156')).toBeDefined()

    const distance = screen.getByText('10.01')
    expect(distance).toBeDefined()
    await userEvent.click(distance)

    expect(mapServiceSpy).toHaveBeenCalled()
    mapServiceSpy.mockRestore()

  })
})
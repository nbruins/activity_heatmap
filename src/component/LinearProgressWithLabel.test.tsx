import { render, screen, waitFor } from '@testing-library/react'
import React from 'react'
import { LinearProgressWithLabel } from './LinearProgressWithLabel'

describe('Test LinearProgress component', () => {

  it('should render with progress set to 5', async () => {

    render(
      <LinearProgressWithLabel value={5} />
    )

    await waitFor(() => { expect(screen.getByText('5%')).toBeDefined(); })
  })
})
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody } from '@mui/material'
import { format } from 'date-fns'
import React, { FC, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { mapService } from '../services'
import { useStoreState } from '../store'

const sxTable = { maxHeight: 240 }
const sxTableRow = { '&:last-child td, &:last-child th': { border: 0 } }

export const TracksTable: FC = () => {

  const { t } = useTranslation()

  const tracks = useStoreState(state => (state.tracks))
  const [selectedTrackId, setSelectedTrackId] = useState<number | null>(null)

  const handleClick = (event: React.MouseEvent<unknown>, id: number | null) => {
    setSelectedTrackId(id)
    if (id)
      mapService.selectTrack(id)
  }

  if (tracks.length === 0)
    return <p>No data</p>

  return (
    <TableContainer component={Paper} sx={sxTable}>
      <Table size="small" stickyHeader aria-label="Tracks">
        <TableHead>
          <TableRow>
            <TableCell>{t('Date')}</TableCell>
            <TableCell align="right">{t('activity')}</TableCell>
            <TableCell align="right">{t('distance')} {t('km')}</TableCell>
            <TableCell align="right">{t('Average speed min/km (km/h)')}</TableCell>
            <TableCell align="right">{t('Duration')}</TableCell>
            <TableCell align="right">{t('Average heart rate (bpm)')}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tracks.map((track) => {

            const speedInMPerS = track.averageSpeedInMPerS ? track.averageSpeedInMPerS : 0
            const speedInKmH = (speedInMPerS * 3.6).toFixed(1).padStart(4, '0')
            const secondsPerKM = Math.floor(1000 / speedInMPerS)
            const speedMinPerKm = format(new Date(secondsPerKM * 1000), 'mm:ss')

            return (
              <TableRow
                key={track.id}
                sx={sxTableRow}
                selected={track.id === selectedTrackId}
                aria-checked={track.id === selectedTrackId}

                onClick={(event) => { handleClick(event, track.id) }}
              >
                <TableCell component="th" scope="row">
                  {new Date(track.timestamp).toDateString()}
                </TableCell>
                <TableCell align="right">{track.activityType}</TableCell>
                <TableCell align="right">{track.distanceInKm}</TableCell>
                <TableCell align="right">{speedMinPerKm} ({speedInKmH})</TableCell>
                <TableCell align="right">{track.duration}</TableCell>
                <TableCell align="right">{track.averageHearthRate}</TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
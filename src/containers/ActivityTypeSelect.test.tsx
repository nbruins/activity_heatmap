import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import '../i18n/i18n'
import { store } from '../store'
import { activityType, tracks } from '../test_mock_data'
import { ActivityTypeSelect } from './ActivityTypeSelect'


beforeAll(() => {
  const addTracksToStore = store.getActions().addTracksToStore
  const addActivityTypes = store.getActions().addActivityTypes

  addTracksToStore({ tracks })
  addActivityTypes(activityType.map(activity => activity.activityType))
})

describe('test ActivityTypeSelect', () => {

  it('should render the ActivityTypeSelect', async () => {

    render(
      <StoreProvider store={store}>
        <ActivityTypeSelect />
      </StoreProvider>
    )

    expect(screen.getByLabelText('Walking')).toBeDefined()
    expect(screen.getAllByRole('button').length).toBe(6)

    const runningButton = screen.getByRole('button', { name: 'Running', pressed: false })
    expect(runningButton).toBeInTheDocument()
    await userEvent.click(runningButton)

    await waitFor(() => { expect(screen.getByRole('button', { name: 'Running', pressed: true })).toBeInTheDocument() })

    await waitFor(() => { expect(fetch).toHaveBeenCalledTimes(2) })

  })

})
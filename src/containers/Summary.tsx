import { Box, Button, List, ListItemButton, ListItemText, Modal, styled, Typography } from '@mui/material'
import { format } from 'date-fns'
import React, { FC, useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useStoreActions, useStoreState } from '../store'
import { Filters } from './Filters'

const StyledDiv = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  marginLeft: 8,
  marginRight: 8,
})

const style = {
  position: 'absolute',
  top: '20%',
  left: '20%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

const displaySettingsList = { display: { xs: 'none', md: 'block' } }
export const Summary: FC = () => {

  const [open, setOpen] = useState(false)

  const { t } = useTranslation()
  const [totalDistanceInKm, tracks, filter, totals] =
    useStoreState(state => ([state.totalDistanceInKm, state.tracks, state.filter, state.totals]))
  const updateFilter = useStoreActions(actions => actions.updateFilter)

  const handleClick = useCallback(() => { setOpen(!open) }, [open])
  const handleOnClick = (year: number) => { updateFilter({ ...filter, dateFrom: new Date(year, 0, 1), dateTo: new Date(year, 11, 31) }) }

  return (
    <StyledDiv>
      <Modal
        open={open}
        onClose={handleClick}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Filters />
        </Box>
      </Modal>
      <Typography component="h2">
        {t(filter.type ?? 'Unknown')}
      </Typography>
      <Button variant='outlined' color="secondary" onClick={handleClick}>
        <Typography component="p">
          {format(filter.dateFrom ?? 0, 'dd-MM-yyyy')} - {format(filter.dateTo ?? 0, 'dd-MM-yyyy')}
        </Typography>
      </Button>
      <Typography component="p">
        {t('total distance')}: {totalDistanceInKm.toFixed(2)} km
      </Typography>
      <Typography component="p">
        {t('activities')}: {tracks.length}
      </Typography>
      <List sx={displaySettingsList}>
        {
          totals.map(total => {
            if (total.activityType === filter.type)
              return (

                <ListItemButton key={total.year} alignItems="flex-start" onClick={() => { handleOnClick(total.year) }}>
                  <ListItemText
                    primary={`${t('Year')}: ${total.year.toString()}`}
                    secondary={
                      <React.Fragment>
                        <Typography

                          sx={{ display: 'inline' }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                        >
                          {t('activities')}: {total.numberOf}; {t('total distance')}: {total.total.toFixed(2)} km
                        </Typography>
                      </React.Fragment>
                    }
                  />
                </ListItemButton>
              )
          })
        }
      </List>
    </StyledDiv>
  )
}
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react'
import { StoreProvider } from 'easy-peasy'
import React from 'react'
import '../i18n/i18n'
import { store } from '../store'
import { activityType, tracks } from '../test_mock_data'
import { Filters } from './Filters'

beforeAll(() => {
  const addTracksToStore = store.getActions().addTracksToStore
  const addActivityTypes = store.getActions().addActivityTypes

  addTracksToStore({ tracks })
  addActivityTypes(activityType.map(activity => activity.activityType))
})

describe('test Filters', () => {

  it('should render the Filters and be able to select date from', async () => {

    render(
      <StoreProvider store={store}>
        <Filters />
      </StoreProvider>
    )


    const fromDate = screen.getByLabelText<HTMLInputElement>('From')
    expect(fromDate).toBeDefined()
    fireEvent.change(fromDate, { target: { value: '2022-03-24' } })
    expect(fromDate.value).toBe('2022-03-24')

    await waitFor(() => { expect(fetch).toHaveBeenCalledTimes(2); })
    // await waitForElementToBeRemoved(() => screen.getByText(/Refreshing/i))
  })

  it('should render the Filters and be able to select date to', async () => {

    render(
      <StoreProvider store={store}>
        <Filters />
      </StoreProvider>
    )

    const toDate = screen.getByLabelText<HTMLInputElement>('To')
    expect(toDate).toBeDefined()
    act(() => {
      fireEvent.change(toDate, { target: { value: '2022-03-24' } })
    })
    await waitFor(() => { expect(fetch).toHaveBeenCalledTimes(2); })
    expect(toDate.value).toBe('2022-03-24')
    // await waitForElementToBeRemoved(() => screen.getByText(/Refreshing/i))
  })
})
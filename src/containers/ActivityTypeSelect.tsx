import { IconButton, styled } from '@mui/material'
import { DirectionsRun, DirectionsBike, DirectionsWalk, DownhillSkiing, Hiking, Pool } from '@mui/icons-material'
import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { useStoreActions, useStoreState } from '../store'
import { SvgIconComponent } from '@mui/icons-material'

const ActivityTypeSelectContainer = styled('div')({
  position: 'absolute',
  zIndex: 999999,
  left: 50,
  top: 10,
  background: '#144519',
  borderColor: '#88A8B2',
  borderWidth: 2,
  borderStyle: 'solid',
  borderRadius: 2,
})

const icons: Record<string, SvgIconComponent> = {
  'Running': DirectionsRun,
  'Walking': DirectionsWalk,
  'Mountaineering': Hiking,
  'Cycling': DirectionsBike,
  'Alpine Skiing': DownhillSkiing,
  'Open Water Swimming': Pool
}

export const ActivityTypeSelect: FC = () => {
  const { t } = useTranslation()

  const [filter, activityTypes] = useStoreState(state => ([state.filter, state.activityTypes,]))
  const updateFilter = useStoreActions(actions => actions.updateFilter)

  const onClick = (type: string): void => {
    updateFilter({ ...filter, type })
  }

  return (
    <ActivityTypeSelectContainer>
      {activityTypes.map(activityType => {
        const Icon = icons[activityType] ?? null
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        if (Icon)
          return (
            <IconButton
              key={activityType}
              aria-label={String(t(activityType))} // is always string because Icon is known
              aria-pressed={filter.type === activityType}
              role="button"
              color={filter.type === activityType ? 'secondary' : 'default'}
              onClick={() => { onClick(activityType) }}
            >
              <Icon titleAccess={String(t(activityType))} />
            </IconButton>
          )
      })}
    </ActivityTypeSelectContainer>
  )
}
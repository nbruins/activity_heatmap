
import {
  Box,
  FormControl, Input,
  InputLabel,
  Slider
} from '@mui/material'
import { format } from 'date-fns'
import React, { FC, useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useStoreActions, useStoreState } from '../store'

export const Filters: FC = () => {

  const { t } = useTranslation()

  const [distanceRange, setDistanceRange] = useState([0, 52])

  const [filter, loading] = useStoreState(state => ([state.filter, state.loading]))
  const updateFilter = useStoreActions(actions => actions.updateFilter)

  const handleChangeDateFrom = useCallback((event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => { updateFilter({ ...filter, dateFrom: new Date(event.target.value) }) }, [filter, updateFilter])

  const handleChangeDateTo = useCallback((event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => { updateFilter({ ...filter, dateTo: new Date(event.target.value) }) }, [filter, updateFilter])

  const handleChangeDistance = useCallback((_event: unknown, value: number | number[]) => {
    setDistanceRange(value as number[])
  }, [])

  const handleChangeDistanceCommitted = useCallback((_event: unknown, value: number | number[]) => {
    updateFilter({ ...filter, distanceRange: (value as number[]) })
  }, [filter, updateFilter])

  return (
    <Box
      component="form"
      sx={{ '& .MuiFormControl-root': { m: 1, width: '25ch' }, 'width': 220 }}
      noValidate
      autoComplete="off"
    >
      {loading && <p>Refreshing</p>}
      <div>
        <FormControl>
          <InputLabel id="date_from-label">{t('from')}</InputLabel>
          <Input
            type='date'
            inputProps={{ 'aria-label': t('from') }}
            value={format(filter.dateFrom ?? 0, 'yyyy-MM-dd')}
            onChange={handleChangeDateFrom}
          />
        </FormControl>
        <FormControl>
          <InputLabel id="date_to-label">{t('to')}</InputLabel>
          <Input
            type='date'
            inputProps={{ 'aria-label': t('to') }}
            value={format(filter.dateTo ?? 0, 'yyyy-MM-dd')}
            onChange={handleChangeDateTo}
          />
        </FormControl>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <InputLabel id="distance-label">{t('distance')}</InputLabel>
          <Slider

            getAriaLabel={() => t('distance range')}
            max={52}
            value={distanceRange}
            onChange={handleChangeDistance}
            onChangeCommitted={handleChangeDistanceCommitted}

            getAriaValueText={(value: number) => `${value.toString()} km`}
            valueLabelDisplay="on"
          />
        </FormControl>
      </div>
    </Box>
  )
}
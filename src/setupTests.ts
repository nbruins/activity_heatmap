import '@testing-library/jest-dom'
import { activityType, baseLayers, tracks } from './test_mock_data'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
globalThis.IS_REACT_ACT_ENVIRONMENT = true

global.fetch = jest.fn((url: string) =>
  Promise.resolve({
    json: () => {

      if (url.includes('/baseLayers'))
        return Promise.resolve({ records: baseLayers })

      if (url.includes('/activityType'))
        return Promise.resolve({ records: activityType })

      if (url.includes('/activity'))
        return Promise.resolve({ records: tracks })

      return Promise.resolve({ records: [] })
    }
  })
) as jest.Mock
## [1.1.2](https://gitlab.com/nbruins/activity_heatmap/compare/1.1.1...1.1.2) (2023-04-08)


### Bug Fixes

* creation of docker container ([9089db1](https://gitlab.com/nbruins/activity_heatmap/commit/9089db1b4a7c45fb2dffe7babbce187d3e406c73))
* Optimize CI and create CHANGES.md ([f9878ff](https://gitlab.com/nbruins/activity_heatmap/commit/f9878ffa27f513977b367687d573373cb4819e1f))

## [1.1.1](https://gitlab.com/nbruins/activity_heatmap/compare/1.1.0...1.1.1) (2023-04-08)


### Bug Fixes

* ISSUE-10: Release notes ([49e3e63](https://gitlab.com/nbruins/activity_heatmap/commit/49e3e634eff7d42065c3a6eaadf44a5d9df7b24a))
* Release notes ([e4fff6d](https://gitlab.com/nbruins/activity_heatmap/commit/e4fff6d09502443d90ffb6166ac929c5eb2b1221))

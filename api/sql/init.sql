SET FOREIGN_KEY_CHECKS=0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `activity_heatmap`
--
-- create the databases
CREATE DATABASE IF NOT EXISTS activity_heatmap;

-- create the users for each database
-- CREATE USER 'activity_user'@'%' IDENTIFIED BY 'Password1!';
-- GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT, INSERT ON `activity_heatmap`.* TO 'activity_user'@'%';

-- FLUSH PRIVILEGES;
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `activity`
--
DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id` int NOT NULL,
  `activityType` varchar(255) NOT NULL,
  `points` json NOT NULL,
  `timestamp` datetime NOT NULL,
  `distanceInKm` float(10,1) NOT NULL,
  `averageSpeedInMPerS` float NOT NULL,
  `averageHearthRate` int,
  `duration` varchar(255) NOT NULL
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `baseLayers`
--
DROP TABLE IF EXISTS `baseLayers`;

CREATE TABLE `baseLayers` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `options` json NOT NULL
) ENGINE=InnoDB;

--
-- Gegevens worden geëxporteerd voor tabel `baseLayers`
--

INSERT INTO `baseLayers` (`id`, `name`, `url`, `options`) VALUES
(1, 'Open Street Map', 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', '{\"maxZoom\": 19, \"attribution\": \"&copy; <a href=\\\"https://www.openstreetmap.org/copyright\\\">OpenStreetMap</a> contributors\"}'),
(2, 'Esri World Imagery', 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', '{\"attribution\": \"Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community\"}'),
(3, 'nlmaps Luchtfoto', 'https://service.pdok.nl/hwh/luchtfotorgb/wmts/v1_0/Actueel_ortho25/EPSG:3857/{z}/{x}/{y}.jpeg', '{\"bounds\": [[50.5, 3.25], [54, 7.6]], \"maxZoom\": 19, \"minZoom\": 6, \"attribution\": \"Kaartgegevens &copy; <a href=\\\"https://www.kadaster.nl\\\">Kadaster</a>\"}'),
(4, 'CartoDB Positron', 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', '{\"maxZoom\": 20, \"subdomains\": \"abcd\", \"attribution\": \"&copy; <a href=\\\"https://www.openstreetmap.org/copyright\\\">OpenStreetMap</a> contributors &copy; <a href=\\\"https://carto.com/attributions\\\">CARTO</a>\"}');


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB;

--
-- Indexen voor tabel `users`
--
-- ALTER TABLE `users`
--   ADD PRIMARY KEY (`id`);

-- --------------------------------------------------------

--
-- Structuur voor de view `activityType`
--
DROP VIEW IF EXISTS `activityType`;

CREATE VIEW `activityType` AS
  SELECT DISTINCT `activity`.`activityType` AS `activityType`
  FROM `activity`;

-- --------------------------------------------------------

--
-- Structuur voor de view `year`
--
DROP VIEW IF EXISTS `year`;

CREATE VIEW `year` AS
  SELECT DISTINCT year(`activity`.`timestamp`) AS `year`
  FROM `activity`;

-- --------------------------------------------------------

--
-- Structuur voor de view `totals`
--
DROP VIEW IF EXISTS `totals`;

CREATE VIEW `totals` AS
  SELECT year(`activity`.`timestamp`) AS `year`, activityType, sum(distanceInKm) AS 'total', count(distanceInKm) AS 'numberOf'
  FROM `activity`
  GROUP BY `year`, activityType;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `baseLayers`
--
ALTER TABLE `baseLayers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `baseLayers`
--
ALTER TABLE `baseLayers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
SET FOREIGN_KEY_CHECKS=1;

COMMIT;
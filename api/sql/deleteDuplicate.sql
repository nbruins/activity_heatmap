SELECT `timestamp`, COUNT(`timestamp`)
FROM
    `activity`
GROUP BY 
    `timestamp`
HAVING 
    COUNT(`timestamp`) > 1;



SELECT sum(`distanceInKm`), count(`distanceInKm`)
FROM `activity`
WHERE `timestamp` >= '20220101'
AND `activityType` = 'Running';



DELETE t1 FROM `activity` t1
INNER JOIN `activity` t2 
WHERE 
    t1.id < t2.id AND 
    t1.timestamp = t2.timestamp;

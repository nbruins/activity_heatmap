<?php
$host = "mysql-server";
$user = "root";
$pass = "secret";
$db = "activity_heatmap";
try {
    $conn = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
    echo "Connected successfully";
    // phpinfo();
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>
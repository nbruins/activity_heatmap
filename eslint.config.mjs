// @ts-check

// import eslint from '@eslint/js'
// import tseslint from 'typescript-eslint'

// export default tseslint.config(
//   eslint.configs.recommended,
//   ...tseslint.configs.strictTypeChecked,
//   ...tseslint.configs.stylisticTypeChecked,
// )



import eslint from '@eslint/js'
import tseslint from 'typescript-eslint'
import * as reactPerf from 'eslint-plugin-react-perf'

export default tseslint.config(
  eslint.configs.recommended,
  ...tseslint.configs.recommendedTypeChecked,
  ...tseslint.configs.strictTypeChecked,
  ...tseslint.configs.stylisticTypeChecked,
  {
    plugins: {
      "react-perf": reactPerf
    },
    languageOptions: {
      parserOptions: {
        projectService: true,
        tsconfigDirName: import.meta.dirname,
      },
    },
    files: ["**/*.ts", "**/*.tsx"],
    ignores: ["**/coverage/"],
    "rules": {
      "@typescript-eslint/await-thenable": "warn"
    }
  }
)
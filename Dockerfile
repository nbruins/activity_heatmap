FROM php:8.1-apache

RUN docker-php-ext-install mysqli pdo pdo_mysql

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY ./php-ini-overrides.ini /usr/local/etc/php/conf.d/